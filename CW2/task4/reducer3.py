#!/usr/bin/python

import sys
import copy





#info for maximum result user
largest_user_id = 0
largest_question_list = []

#a convenient way to print the result
def flush_list():
	result = "{} -> {}".format(largest_user_id, len(largest_question_list))
	for item in largest_question_list:
		result += ", {}".format(item)

	print result

#info for current user
prev_user_id = None
question_list = []

#clear the list
def clear_questions():
	for x in xrange(len(question_list)):
		question_list.pop()
     


   
for line in sys.stdin:

    #get relevant info
    line = line.strip()
    user_id, value = line.split("\t")
    values = value.split() 

    #detect info for current user ends
    if user_id != prev_user_id or prev_user_id is None:

	if prev_user_id is not None:
  		#attempt to get larger result
		if len(question_list) > len(largest_question_list):

			largest_user_id = prev_user_id
			largest_question_list = list(question_list)

		clear_questions()
		

        prev_user_id = user_id

    #add values to the list
    question_list.extend(values)
	
     
#attempt to get larger result
if len(question_list) > len(largest_question_list):
	largest_user_id = prev_user_id
	largest_question_list = list(question_list)
	clear_questions()

#print maximum result 
flush_list()









