#!/usr/bin/python

import sys
import copy





#info for the maximum result user
largest_user_id = 0
largest_question_list = []

#convenient way to print the list
def flush_list():
	result = "{}\t".format(largest_user_id)
	for item in largest_question_list:
		result += "{} ".format(item)

	print result

#id of currently considered user
prev_user_id = None
#list containing all accepted answers by user (their questions)
question_list = []

#clear the list
def clear_questions():
	for x in xrange(len(question_list)):
		question_list.pop()
     
   
for line in sys.stdin:

    #get releavn info
    line = line.strip()
    user_id, value = line.split("\t")
    values = value.split() 


    #detect when info for current user ends
    if user_id != prev_user_id or prev_user_id is None:

	if prev_user_id is not None:
		#attempt to get new largest result
		if len(question_list) > len(largest_question_list):
			largest_user_id = prev_user_id
			largest_question_list = list(question_list)

		clear_questions()
		

        prev_user_id = user_id

    #add values to the list
    for string in values: 	
    	question_list.append(string)
	
     
#last attempt to get new largest result
if len(question_list) > len(largest_question_list):
	largest_user_id = prev_user_id
	largest_question_list = list(question_list)
	#print largest_question_list
	clear_questions()

#print the result
flush_list()









