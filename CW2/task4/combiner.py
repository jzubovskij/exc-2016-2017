#!/usr/bin/python

import sys



prev_answer_id = None

question_id = "None"
user_id = "None"

#convenient way to print the line
def print_line():
	if prev_answer_id != None:
		print "{}\t{} {}".format(prev_answer_id, question_id, user_id)

for line in sys.stdin:

    #get relevant info
    line = line.strip()
    answer_id, value = line.split("\t")	
    values = value.split()

    #detect when accepted_anwer id changes
    if answer_id != prev_answer_id:
	print_line()
	question_id = "None"
	user_id = "None"

    prev_answer_id = answer_id
 
    #if have values to record, record them while not ovewriting old (possibly valid) ones 
    if values[0].strip() != "None":
	question_id = values[0].strip()
    if values[1].strip() != "None":
	user_id     = values[1].strip()


#print the list
print_line()
    	








