#!/usr/bin/python

import sys



prev_answer_id = None

question_id = None
user_id = None

#convenient way to print the line
def print_line():
	#only interested in answers that have questions and visa versa
	if prev_answer_id is not None and (question_id is not None and user_id is not None):
		print "{}\t{}".format(user_id, question_id)

for line in sys.stdin:

    #get relevant info
    line = line.strip()
    answer_id, value = line.split("\t") 	
    values = value.split()

    #detect answer id change
    if answer_id != prev_answer_id:
	print_line()
        question_id = None
        user_id = None


    prev_answer_id = answer_id
    
    #if have values to record, record them while not ovewriting old (possibly valid) ones 
    if values[0].strip() != "None":
	question_id = values[0].strip()
    if values[1].strip() != "None":
	user_id     = values[1].strip()


#print remainign items
print_line()
    	








