#!/usr/bin/python

import sys
import xml.etree.ElementTree as ET

for line in sys.stdin:

    #parse entry into a dictionary
    line = line.strip()
    xml_dictionary = ET.fromstring(line).attrib

    #interested in questions
    if int(xml_dictionary["PostTypeId"]) == 1:
    	Id =  xml_dictionary["Id"]
	try:
    		AcceptedAnswerId = xml_dictionary["AcceptedAnswerId"]
	except:
		#do not print questions without accepcted answers
		continue

	#indicate it is a answer
	print "{}\t{} {}".format(AcceptedAnswerId, Id, "None")
   
    #interested in answers	
    elif int(xml_dictionary["PostTypeId"]) == 2:
    	OwnerUserId =  xml_dictionary["OwnerUserId"]
    	Id = xml_dictionary["Id"]
	#indicate it is a answer
	print "{}\t{} {}".format(Id, "None" , OwnerUserId)
   








