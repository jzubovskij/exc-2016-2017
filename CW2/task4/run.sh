#!/bin/bash

#the first map reduce, aggregating question and accepted answer user's id

hdfs dfs -rm -r /user/s1346981/assignment2/task4_1/ 
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 4.1  s1346981" \
-input /data/assignments/ex2/part2/stackLarge.txt \
-output /user/s1346981/assignment2/task4_1/ \
-mapper mapper.py \
-combiner combiner.py \
-reducer reducer.py \
-file mapper.py \
-file combiner.py \
-file reducer.py 

#the second map reduce, producing partial results

hdfs dfs -rm -r /user/s1346981/assignment2/task4_2/ 
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 4.2 s1346981" \
-input /user/s1346981/assignment2/task4_1/  \
-output /user/s1346981/assignment2/task4_2/ \
-mapper cat \
-combiner combiner2.py \
-reducer reducer2.py \
-file combiner2.py \
-file reducer2.py 

#the third map reduce meant to make sure we do not send all our data into one reducer in the 2nd mapreduce
#hence, this one is used to partially determine the most-accepted-answer-users (one per each reducer)
#this one aggregates those results to produce the final result - the user with the most accepted answers

hdfs dfs -rm -r /user/s1346981/assignment2/task4/ 
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 4.3 s1346981" \
-D mapreduce.job.reduces=1 \
-input /user/s1346981/assignment2/task4_2/  \
-output /user/s1346981/assignment2/task4/ \
-mapper cat \
-reducer reducer3.py \
-file reducer3.py 




