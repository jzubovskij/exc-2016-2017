#!/usr/bin/env python
import sys
import random

total_line_count = 0
reservoir = None

for line in sys.stdin:
    line = line.strip()
	

    tokens = line.split("\t")

    sample = tokens[0]
    line_count = int(tokens[1])

    total_line_count += line_count

    #scale the weight of probability for each sample by n/N, where n is the number of lines it has been selected from (in the mapper)
    #and N is the total number of lines that the incoming lines (their sum)
    if random.randint(0, total_line_count-1) < line_count:
        reservoir = sample.strip()

print reservoir
