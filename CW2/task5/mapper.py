#!/usr/bin/env python
import sys
import random

line_number = 0
reservoir = None

for line in sys.stdin:
    line = line.strip()
    #only with probability 1/n replace
    if random.randint(0, line_number) == 0:
        reservoir = line
    line_number += 1

#print the reservoir and the number of lines it was selected from
print("{}\t{}".format(reservoir, line_number))
