#!/bin/bash
hdfs dfs -rm -r /user/s1346981/assignment2/task5/ 
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 5 s1346981" \
-D mapreduce.job.reduces=1 \
-input /data/assignments/ex2/part3/webLarge.txt \
-output /user/s1346981/assignment2/task5/ \
-mapper mapper.py \
-reducer reducer.py \
-file mapper.py \
-file reducer.py 


