#!/usr/bin/python

import sys
import json
import os

#maximum size of the dictionary
MAX_SIZE = 1000

#dictionary to that holds word occurences in the current file
dictionary = dict()


file_name = ""

#increment value in dictionary or add to it (generalized method)
def incr_dict(key, value):
        global size
	if key in dictionary:
	      #increment
	      dictionary[key]+= value
	else:
	      #add new record
	      dictionary[key] = value


#print items in the dictionary
def flush_dictionary():
	#print all items 
	for key, value in dictionary.items():
		print "{}\t{} {}".format(key, file_name , value)

	dictionary.clear() 

def check_full():
    #only keep a bounded number of records in memory
    if  len(dictionary) > MAX_SIZE:
	#flush all values
	flush_dictionary()


for line in sys.stdin:
    
    #get the file name
    file_name = os.environ["mapreduce_map_input_file"].split("/")[-1]	 	 
    line = line.strip()

    #do not accept empty lines
    if len(line) == 0:
	continue

    #get the words we need to count the occurences of
    keys  = line.split()

    #increment their counts in the dictionary
    for key in keys:
	incr_dict(key.strip(), 1)
	check_full()


#print remainign items
flush_dictionary()
    	
