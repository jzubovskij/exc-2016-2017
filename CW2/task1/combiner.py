#!/usr/bin/python

import sys
import json

#maximum size of the dictionary
MAX_SIZE = 1000

#dictionary to holding file names and word occurences
dictionary = dict()

prev_word = None

#increment value in dictionary or add to it (generalized method)
def incr_dict(key, value):
	if key in dictionary:
	      #increment
	      dictionary[key]+= value
	else:
	      #add new record
	      dictionary[key] = value
	      #record size increase

#make string to conveniently print the dictionary
def make_dict_string():
	result = ""
	for key, value in dictionary.items():
		result+= "{} {} ".format(key, value)

	return result


#print items in the dictionary
def flush_dictionary():
	dict_string = make_dict_string()
	print "{}\t{}".format(prev_word, dict_string)
	# reset dictionary
	dictionary.clear()

def check_full():
    #only keep a bounded number of records in memory
    if  len(dictionary) > MAX_SIZE:
	#flush all values
	flush_dictionary()

for line in sys.stdin:


 
    line = line.strip()
    word, value  = line.split("\t")
    
    file_name, count = value.split()

    #if it is the next word, pritn current dictionary
    if word.strip() != prev_word or prev_word is None:
	
	if prev_word is not None:
		flush_dictionary()

	prev_word = word.strip()

    #add values to dictionary

    incr_dict(file_name.strip() , int(count))
    #check if full
    check_full()

#print remainign items
flush_dictionary()
    	
