#!/bin/bash
hdfs dfs -rm -r /user/s1346981/assignment2/task1/ 
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 1 s1346981" \
-input /data/assignments/ex2/part1/large/ \
-output /user/s1346981/assignment2/task1/ \
-mapper mapper.py \
-combiner combiner.py \
-reducer reducer.py \
-file mapper.py \
-file reducer.py \
-file combiner.py 




