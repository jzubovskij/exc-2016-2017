#!/usr/bin/python

import sys

#dictionary to that holds the files and occurences of the word
dictionary = dict()

prev_word = None

#increment value in dictionary or add to it (generalized method)
def incr_dict(key, value):
	if key in dictionary:
	      #increment
	      dictionary[key]+= value
	else:
	      #add new record
	      dictionary[key] = value

#the final output dictionary string obtainer
def make_dict_string():

	index = 0
	result = ""
	
        #sort dictionary as per output requirement
	sorted_dictionary = sorted(dictionary.items())

	for item in sorted_dictionary:
		if index != len(dictionary) - 1:
			result+= "({},{}),".format(item[0], item[1])
		else:
		        result+= "({},{})".format(item[0], item[1])	
		index += 1

	return result


#print items in the dictionary
def flush_dictionary():

	dict_string = make_dict_string()
	print "{} : {} : {{{}}}".format(prev_word, len(dictionary), dict_string)

	dictionary.clear() 

for line in sys.stdin:
  
 
    line = line.strip()
    word, value  = line.split("\t")
    
    
    values = value.split()


    #if it is the next word, pritn current dictionary
    if word != prev_word or prev_word is None:

	if prev_word is not None:
		flush_dictionary()

	prev_word = word


    #add values to dictionary
    for x in xrange(0, len(values), 2):

	key = values[x]
	value = values[x+1]	

	incr_dict(key , int(value))

#print remainign items
flush_dictionary()
    	
