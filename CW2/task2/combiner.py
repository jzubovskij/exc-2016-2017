#!/usr/bin/python

import sys
from heapq import heappush, heappop, nlargest

top10 = []
MAX_LEN = 1000

for line in sys.stdin:


    line = line.strip()
    tokens = line.split()

    #get relevant information for question
    Id =  tokens[1]
    ViewCount = tokens[0]

    #push onto priority queue
    heappush(top10, (int(ViewCount), Id))

    #remove from the queue if it is overflowing
    if len(top10) > MAX_LEN:
	for x in xrange(MAX_LEN - 10):
		heappop(top10)
		
	
   
#print the top 10 items
for item in nlargest(10, top10):
	print "{}\t{}".format(item[0] , item[1])
