#!/usr/bin/python

import sys
from heapq import heappush, heappop, nlargest

top10 = []
MAX_LEN = 1000

for line in sys.stdin:


    line = line.strip()
    tokens = line.split()

    #get relevant info
    Id =  tokens[1]
    ViewCount = tokens[0]


    #put it into the priority queue
    heappush(top10, (int(ViewCount), Id))

    if len(top10) > MAX_LEN:
	for x in xrange(MAX_LEN - 10):
		heappop(top10)
		
	   
#print the top 10 items
for item in nlargest(10, top10):
	print "{} {}".format(item[0] , item[1])
