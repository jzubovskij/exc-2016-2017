#!/usr/bin/python

import sys
from heapq import heappush, heappop, nlargest

import xml.etree.ElementTree as ET

top10 = []
MAX_LEN = 1000

for line in sys.stdin:


    #parse the entry into a dictionary
    line = line.strip()
    dictionary = ET.fromstring(line).attrib

    #only interested in questions
    if int(dictionary["PostTypeId"]) != 1:
	continue

    #get relevant question info
    Id =  dictionary["Id"]
    ViewCount = dictionary["ViewCount"]
   
    #push nto priority queue
    heappush(top10, (int(ViewCount), Id))

    #if too many items inserted, remove the lowest counts
    if len(top10) > MAX_LEN:
	for x in xrange(MAX_LEN - 10):
		heappop(top10)
		
	
#print the top 10 items
for item in nlargest(10, top10):
	print "{}\t{}".format(item[0] , item[1])
