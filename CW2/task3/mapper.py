#!/usr/bin/python

import sys
import xml.etree.ElementTree as ET


#maximum size of the dictionary
MAX_SIZE = 1000

#dictionary to that holds the sets of answers for each user
dictionary = dict()

#increment value in dictionary or add to it (generalized method)
def incr_dict(key, value):

	if key in dictionary:
	      #increment
	      dictionary[key].add(value)
	else:
	      #add new record
	      dictionary[key] = set([value])

#get string to print a set
def get_set_string(a_set):
	result = ""
	for item in a_set:
		result += "{} ".format(item)
	return result

#print items in the dictionary
def flush_dictionary():
	#print all items 
	for key, value in dictionary.items():
		print "{}\t{}".format(key, get_set_string(value))

	dictionary.clear() 

def check_full():
    #only keep a bounded number of records in memory
    if  len(dictionary) > MAX_SIZE:
	#flush all values
	flush_dictionary()




for line in sys.stdin:

    #parse entry into a dictionary
    line = line.strip()
    xml_dictionary = ET.fromstring(line).attrib

    #only interested in answers
    if int(xml_dictionary["PostTypeId"]) != 2:
	continue
    
    #get relevant info
    OwnerUserId =  xml_dictionary["OwnerUserId"]
    ParentId = xml_dictionary["ParentId"]
    
    #add values into dictionary
    incr_dict(OwnerUserId, ParentId)
    check_full()


#print remainign items
flush_dictionary()
    	








