#!/bin/bash


#the first mapreduce, used for partial resutl aggegations

hdfs dfs -rm -r /user/s1346981/assignment2/task3_1/ 
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 3.1 s1346981" \
-input /data/assignments/ex2/part2/stackLarge.txt \
-output /user/s1346981/assignment2/task3_1/ \
-mapper mapper.py \
-combiner combiner.py \
-reducer reducer.py \
-file mapper.py \
-file combiner.py \
-file reducer.py 

#the second map reduce meant to make sure we do not send all our data into one reducer in the first mapreduce
#allowing the reducers in the first map reduce to obtain partial results and send them into the second map reduce
#therefore, aggregating the final result in the second map reduce

hdfs dfs -rm -r /user/s1346981/assignment2/task3/ 
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 3.2 s1346981" \
-D mapreduce.job.reduces=1 \
-input /user/s1346981/assignment2/task3_1/ \
-output /user/s1346981/assignment2/task3/ \
-mapper cat \
-reducer reducer2.py \
-file reducer2.py 


