#!/usr/bin/python

import sys

#maximum size of the dictionary
MAX_SIZE = 1000

#set holding the answers for currenly considered user
post_set = set()

prev_poster = None

def get_set_string(a_set):
	result = ""
	for item in a_set:
		result += "{} ".format(item)
	return result

#print items in the set
def flush_set():
	#print all items 
	print "{}\t{}".format(prev_poster, get_set_string(post_set))
	post_set.clear() 

def check_full():
    #only keep a bounded number of records in memory
    if  len(post_set) > MAX_SIZE:
	#flush all values
	flush_set()




for line in sys.stdin:


    #get relevant info
    line = line.strip()
    poster, posts_string = line.split("\t")
    posts = posts_string.split()
    
    #detect when the user / poster changes
    if poster != prev_poster or prev_poster is None:
	if prev_poster is not None:
		flush_set()

        prev_poster = poster

    #add values to the set
    for post in posts:
	post_set.add(post)
    	check_full()


#print remainign items
flush_set()
    	








