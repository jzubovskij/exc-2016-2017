#!/usr/bin/python

import sys

#maximum size of the dictionary
MAX_SIZE = 1000

#current size of the dictionary
size = 0

#info for current user
post_set = set()
prev_poster = None

#info for maximum result
largest_post_set = set()
largest_poster = None


#convenient way to print a set
def get_set_string():

	result = ""
	for item in largest_post_set:

		result += "{} ".format(item)

	return result

#print items in the set
def flush_set():
	#print all items 
	print "{}\t{}".format(largest_poster, get_set_string())
	post_set.clear() 


for line in sys.stdin:

    #get relevant info
    line = line.strip()
    poster, posts_string = line.split("\t")
    posts = posts_string.split()
    
    #check when the poster id changes
    if poster != prev_poster or prev_poster is None:

        #check if longer result detected
	if len(post_set) > len(largest_post_set):
		largest_post_set = post_set.copy()
		largest_poster   = prev_poster

	post_set.clear()
        prev_poster = poster

    #add posts into the set
    for post in posts:
	post_set.add(post)


#check for longest list the last time
if len(post_set) > len(largest_post_set):
	largest_post_set = post_set.copy()
	largest_poster   = prev_poster

#print result
flush_set()

    	








