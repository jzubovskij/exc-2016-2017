#!/usr/bin/python

import sys
import math
from bitarray import bitarray


#calculate the filter parameters
n = int(sys.argv[1])
ln2 = math.log(2)
m = int(math.ceil(-n*math.log(0.01)/math.pow(ln2, 2)))
k = int(math.ceil(m * ln2 / float(n)))

#make use of the bit array
bloom_filter = bitarray(m)
bloom_filter.setall(False) 

for line in sys.stdin:
    line = line.strip()

    duplicate = True
    #hash according to number of hashes
    for x in xrange(k):
        #has the value with a padding of the index
        bit_index = hash(str(x+1) + "_" +line) % m
	#if at least one of the bits is not set, then we can say it is not a duplicate (present in the set)
        duplicate = duplicate and bloom_filter[bit_index]
	#set releavnt bits
        bloom_filter[bit_index] = True
    
    #print if not a duplicate	
    if not duplicate:
        print line   









