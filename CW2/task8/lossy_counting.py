#!/usr/bin/python

import sys
import math

e = 0.001
w = math.ceil(1 / e)
s = 10*e

bucket = dict()
bucket_id = 0

line_number = 0




#increase value frequency on the bucket
def incr_bucket(key):
    if key in bucket:
        bucket[key][0] += 1
    else:
        bucket[key] = [1, bucket_id - 1]

#remove low entries from the bucket
def refresh_bucket():
    for key in bucket.keys():
        element = bucket[key]
        remove = (element[0] + element[1]) <= bucket_id
        if remove:
            del bucket[key]

#print the dictionary's items as per rule
def request_bucket():
    for key in bucket.keys():
        element = bucket[key]
        to_print = (element[0] >= (s-e)*line_number)
        if to_print:
            print "{}".format(key)
           
#method to calculate new bucket
def calculate_bucket_id():
    bucket_id = int(math.ceil(line_number/w))

for line in sys.stdin:
    line_number += 1
    #refresh bucket id
    calculate_bucket_id()
    if line_number % int(w) == 0:
	
	#get rid of low entries
	refresh_bucket() 
    
    element = line.strip()
    incr_bucket(element)

#print the requested results
request_bucket()









