#!/usr/bin/python

import sys

line_number = 0

for line in sys.stdin:
    line = line.strip()

    #as input sorted in descending numerical order with the count being the key, we only want the first 20 lines
    if line_number < 20:
    	print(line)
	line_number += 1
    #other lines are just consumed as per hadoop requirement
