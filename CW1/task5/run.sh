#!/bin/bash
hdfs dfs -rm -r /user/s1346981/assignment1/task5/
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapreduce.job.output.key.comparator.class=org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
-D mapred.job.name="Task 5 s1346981" \
-D mapreduce.partition.keycomparator.options=-nr \
-D mapreduce.job.reduces=1 \
-input /user/s1346981/assignment1/task4/ \
-output /user/s1346981/assignment1/task5/ \
-mapper mapper.py \
-reducer reducer.py \
-combiner combiner.py \
-file mapper.py \
-file reducer.py \
-file combiner.py 





