#!/usr/bin/python

import sys
import re


#only send the top 20 to save bandwidth (as we are already traversing them lineraly might as well choose the top 20 most occuring)
list_key   = [0] * 20
list_value = [""]*20


#check if current key (count) is bigger than any of the keys (counts currently in the dictionary
def check_insert(key, value):

    #check if have found a more frequent combination than one of the recorded ones
    lowest_index = list_key.index(min(list_key))
    if list_key[lowest_index]  < key:

	#if that is the key, replace the lowest key , value pair with current one
	list_key[lowest_index]   = key
	list_value[lowest_index] = value
				
			

for line in sys.stdin:

    
    line = line.strip()
    tokens = line.split('\t') 
    #number of occurences is the key
    check_insert(int(tokens[1]), tokens[0])

#print the results
for index in xrange(20):
    #print count of occurence first to use -nr sorting
    print '%s\t%s' % (str(list_key[index]), list_value[index])
