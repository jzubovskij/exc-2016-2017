#!/bin/bash
hdfs dfs -rm -r /user/s1346981/assignment1/task7/
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 7 s1346981" \
-input /data/assignments/ex1/uniLarge.txt \
-output /user/s1346981/assignment1/task7/ \
-mapper mapper.py \
-reducer reducer.py \
-combiner combiner.py \
-file mapper.py \
-file reducer.py \
-file combiner.py 






