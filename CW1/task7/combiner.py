#!/usr/bin/python

import sys
import json

#dictionary of associated (subject,grade) and (name, actual_name) items
dictionary = dict()

#current student id (as in all combiners and reducers before, sorted by the key, so can just aggregate all values for one id then go to the next one)
prev_key =  None


def make_stripe(key, values):
	#use student id as key for sorting
	result = str(key) + "\t"
	#use json to print the dictionary
	result += json.dumps(values)
	return result

#add value to dictionary held as a value
def incr_dict(dictionary, key, value):
	#if value exists, add to it 
	if key in dictionary:
	      dictionary[key]+= value
	else:
	#otherwise add new record and increment size accordingly
	      dictionary[key] = value

for line in sys.stdin:

    line = line.strip()
    #know the format, so can split liek this
    tokens = line.split('\t')

    #key is student id
    key = tokens[0]
    
    #if a new student, otput values related to current student (as values sorted by id)
    if key != prev_key and prev_key != None:
	#print aggregated values for current student id (no actual parsing done)
	print(make_stripe(prev_key, dictionary))
	dictionary.clear() 
                
    #record current id
    prev_key = key    

    #read the dictionary and add its values to current one
    associated_dictionary = json.loads(tokens[1])

    for item, value in associated_dictionary.items():
	incr_dict(dictionary, item, value) 
	
#print the remaining records
print(make_stripe(prev_key, dictionary))


