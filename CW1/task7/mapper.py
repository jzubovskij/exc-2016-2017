#!/usr/bin/python

import sys
import json

MAX_SIZE  = 1000
record_number = 0

#dictionary of dictionaries, key = student id and a value is a dictionary of associated (subject,grade) and (name, actual_name) items
dictionary = dict()

#make a stripe out of the data
def make_stripe(key, values):
	#use student id as the key for sorting (use tab to inform hadoop without any additional directives as all the times before)
	result = str(key) + "\t"
	#use json for easy printing
	result += json.dumps(values)
	return result

#add value to dictionary held as a value
def incr_dict(dictionary, key, value):
	global record_number
	#if value exists, add to it 
	if key in dictionary:
	      dictionary[key]+= value
	else:
	#otherwise add new record and increment size accordingly
	      dictionary[key] = value
	      record_number += 1

#check addition to main dictionary	   
def incr_primary_dict(key, value, value_two):
	global record_number
        #unlike in previous mapppers that use dictionary, here the value can be a number or a string, so keeping it general
	if key in dictionary:
	      #add record to existing key
	      incr_dict(dictionary[key], value, value_two)
	else:
	      #create new dictionary otherwise
	      dictionary[key] = dict()
	      incr_dict(dictionary[key], value, value_two)

def check_full():
    global record_number
    #remove overflow in values
    if record_number > MAX_SIZE :
	#pop item to reduce record count
	key, value = dictionary.popitem()
	#value here is a dictionary in itself, so reduce record count by its length
	record_number -= len(value)
	#print the stripe
	stripe = make_stripe(key, value)
	print(stripe)


for line in sys.stdin:

    
    line = line.strip()
    tokens = line.split()

    #if it is a mark, record it as a mark for the student id
    if tokens[0] == 'mark':
	#the key is student id
	key = int(tokens[1])
	value = tokens[2]
	sec_value = int(tokens[3])
  	incr_primary_dict( key, value, sec_value)
    #otherwise record it as a name for student id
    else:
	#the key is student id
	key = int(tokens[1])
	#special key for name to keep all values related to student id in one dictionary
	value = "name" 
	sec_value = tokens[2]
   	incr_primary_dict(key, value, sec_value)

    #check if over memory bounds
    check_full()
	
#print the remaining records
for key, value in dictionary.items():
	print(make_stripe(key, value))	


