#!/usr/bin/python

import sys
import json

dictionary = dict()
prev_key =  ""

#NOTE: same as in other reducers (and combiners) no memory bounds on structures where valeus associated with current key 
#because we cannot print anything until correctly agregate all values for them


#add value to dictionary held as a value
def incr_dict(dictionary, key, value):
	#if value exists, add to it 
	if key in dictionary:
	      dictionary[key]+= value
	else:
	#otherwise add new record and increment size accordingly
	      dictionary[key] = value

def print_items():
	#since name is also part of the dictionary as a key, need to treat it differently
	result = dictionary["name"] + " --> "
 	#print the subjects and their marks
	for key, value in dictionary.items():
		if key != "name":
			#output as shown in sample output
			result += "(%s,%s)  " %  (key, value)
	print(result)

for line in sys.stdin:



    line = line.strip()
    tokens = line.split('\t')

    key = tokens[0]
    

    #if a new student, print current student's information as student sorted by id, if id changes, all info for current student (id) ended
    if key != prev_key and not (prev_key ==  ""):
	#print items, reset variables
	print_items()
	dictionary.clear() 
    
    #record new id            
    prev_key = key    

    #load the received dicionary's values into our current one and append where appropriate
    associated_dictionary = json.loads(tokens[1])

    for item, value in associated_dictionary.items():
	incr_dict(dictionary, item, value) 
	
#print the remaining records
print_items()


