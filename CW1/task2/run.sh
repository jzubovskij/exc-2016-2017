#!/bin/bash
hdfs dfs -rm -r /user/s1346981/assignment1/task2/
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 2 s1346981" \
-input /user/s1346981/assignment1/task1/ \
-output /user/s1346981/assignment1/task2/ \
-mapper mapper.py \
-combiner combiner.py \
-reducer reducer.py \
-file mapper.py \
-file combiner.py \
-file reducer.py 

