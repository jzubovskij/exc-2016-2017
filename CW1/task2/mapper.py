#!/usr/bin/python

import sys

prev_line = None
prev_repeats = False
initial = True


#print previous line if it was only encountered once, the idea is to ensure partial aggregation 
#does not mess up full aggregation - say 2 lines "A" got to one combiner and one line "A" got 
#another one - this ensures that is detected by the reducer
def print_line():
	if prev_line is not None:
		result = str(prev_line)	
		if not prev_repeats:
		    result += "\t-"
		else:
		    result += "\t+"
		print(result)

for line in sys.stdin:
    #rstrip to not kill the first token in the line (empty if empty line)
    line = line.rstrip()    

    #if line repeats, record that
    if line == prev_line:
	prev_repeats = True
    #otherwise a different line was encountered
    else:

	
	print_line()

	#reset the state
	prev_repeats = False
	prev_line = line	                	

#print las line if it did not repeat
print_line()


