#!/usr/bin/python

import sys

prev_line = None
prev_repeats = False
initial = True


def print_line():
	if prev_line is not None:
		result = str(prev_line)	
		if not prev_repeats:
		    print(result)


for line in sys.stdin:

    #rstrip to not kill the first token in the line (empty if empty line)
    line = line.rstrip()
    
    repeat_indicator = False    
    #if previously detected to repeat
    tokens = line.split('\t')
    if tokens[1] == "+":
	repeat_indicator = True

    #make line to be as before
    line = str(tokens[0])

    #if line repeats, record that
    if line == prev_line:
	prev_repeats = True
    #otherwise a different line was encountered
    else:

	print_line()
	
	#reset the state
	prev_repeats = False
	prev_line = line
    
    #update the repeat indicator for current line
    prev_repeats = repeat_indicator or prev_repeats        	

#print las line if it did not repeat
print_line()
	
	


