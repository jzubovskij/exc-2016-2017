#!/usr/bin/python

import sys

#only send out the longest token and line length values
longest_token = 0
longest_line  = 0 

for line in sys.stdin:
    #remove stuff not counter into line length
    line = line.strip()
    #determine new longets line
    longest_line = max(len(line),longest_line)
    tokens = line.split()
     
    #get longest token in the line
    maxlength = max(len(string) for string in tokens) 
    #determine new longest token
    longest_token = max(maxlength , longest_token)
	
#only send out length of longest token and line
print '%d\t%d' % (longest_token, longest_line)
                	





