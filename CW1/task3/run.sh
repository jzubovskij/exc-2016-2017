#!/bin/bash
hdfs dfs -rm -r /user/s1346981/assignment1/task3/
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 3 s1346981" \
-D mapreduce.job.reduces=1 \
-input /user/s1346981/assignment1/task2/ \
-output /user/s1346981/assignment1/task3/ \
-mapper mapper.py \
-reducer reducer.py \
-combiner combiner.py \
-file reducer.py \
-file combiner.py \
-file mapper.py 

