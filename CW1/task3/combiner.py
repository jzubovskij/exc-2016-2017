#!/usr/bin/python

import sys

#only send out the longest token and line length values
longest_token = 0
longest_line  = 0 

for line in sys.stdin:
    #remove stuff not related to line length
    line = line.strip()

    tokens = line.split()
    
    #determine new longest token and line
    longest_line = max(int(tokens[1]),longest_line)
    longest_token = max(int(tokens[0]), longest_token)
	
#only send out length of longest token and line
print '%d\t%d' % (longest_token, longest_line)
                	





