#!/usr/bin/python
import sys

#keep track of highest average and associated names
highest_average = 0
highest_average_names = []


for line in sys.stdin:

    line = line.strip()
    tokens = line.split("\t")

    average = float(tokens[0])

    #do not waste computational power if lower than current highest average
    if average < highest_average:
    	continue 

    #we know they are separated by a tab from the key (their average)
    names = tokens[1].split()

    #add name to list if his / her average equal to current highest average
    if average == highest_average:
	 highest_average_names.extend(names)
    #remove all previous names from the list
    elif average > highest_average:
	#make new list out of the current highest performing students
	highest_average_names = names
	highest_average = average
    
#print the key  (higherst average) and the related names from this combiner
result = str(highest_average)  + "\t"  
for index in xrange(0, len(highest_average_names)):
	result += highest_average_names[index]  + " "

print(result)

