#!/usr/bin/python
import sys

#keep track of highest average and associated names
highest_average = 0
highest_average_names = []


for line in sys.stdin:

    #parse input as from part 7
    line = line.strip()
    tokens = line.split("-->")

    #get student name
    name = tokens[0]
    
    subjects = tokens[1].split()
    subject_count = len(subjects) 

    #only care for those that have > 3 subjects, do not waste computational power
    if(subject_count <= 3):
    	continue

    sum_grades = 0

    #parse the grades out of the text
    for index in xrange(0, len(subjects)):

	stripped_paranthesis = subjects[index].strip(")")
	#print(stripped_paranthesis )
	
 	#have strings like "(subject, grade", so separate on the comma and take the grade
	grade_string = stripped_paranthesis.split(",")[1]

	#sum the grades
	sum_grades += int(grade_string)
    
    #calculate the average
    average = sum_grades / float(subject_count)
  

    #add name to the list if equal average as highest one recorded
    if average == highest_average:
	 highest_average_names.append(name)
    #remove all previous names from the list if found a new highest average
    elif average > highest_average:
	#and start a new list with only the new list in it
	highest_average_names = [name]
	highest_average = average
    
#print the key  (higherst average) and the related names from this mapper (so hadoop sorts by average)
result = str(highest_average)  + "\t"  
for index in xrange(0, len(highest_average_names)):
	result += highest_average_names[index] + " "

print(result)

    
