#!/usr/bin/python

import sys
import json

#as input sorted by context (first two words), just keep the occurences of words coming with the context
words = []

#the current context we are considering and recording values for
prev_key =  ""

#make a stripe of the key(first two words) and their associated list of counts of occurences
def make_stripe(key, values):
	#use context as the key to again be sorted by it
	result = key + "\t"
	#as we did no parsing, make sure numbers do not glue together by accident, add a space
	for index in xrange(0, len(values)):
		result += str(values[index]) + " "
	return result 

#print current ("prev") key and associated list of counts of occurences
def print_items():
	print(make_stripe(prev_key, words))

for line in sys.stdin:

    line = line.strip()
    tokens = line.split('\t')
    #as we know that that is how the context is separated from the list
    key = tokens[0]

    

    #if it is a new key and it is not an initial one, then the print items
    if key != prev_key and not (prev_key ==  ""):
	#clear the list containing the occurences of the 3rd words for current context
	print_items()
	words = [] 
    
    #update key and add occurence to list            
    prev_key = key    
    words.append(tokens[1])
	
#print the remaining records
print_items()




