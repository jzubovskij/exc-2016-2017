#!/usr/bin/python

import sys

#maximum size of the dictionary
MAX_SIZE = 1000

#current size of the dictionary
size = 0

#dictionary (of lists) holding for each key = context (2 words) a list of occurences of a 3rd one (do not care waht they were to calculate entropy)
dictionary = dict()

#get the dictionary size
def dictionary_size(dictionary):
	result = sys.getsizeof(dictionary)
	return result

#make a stripe of the key(first two words) and their associated list of counts of occurences
def make_stripe(key, values):
	#tab is the separator so hadoop sorts by key
	result = key + "\t"
	#first two words are the key
	for index in xrange(0, len(values)):
		result += str(values[index]) + " "
	return result 

#increment or add value into the dictionary of lists
def incr_primary_dict(key, value):
	global size
	if key in dictionary:
	      #append the occurence to existing list and increment record count
	      dictionary[key].append(value)
	      size += 1
	else:
	      #create a new list with the value in it and increment record count
	      dictionary[key] = [value]
	      size += 1

def check_full():
    #only keep a bounded number of records in memory
    global size
    if  size > MAX_SIZE:
	#if size too big, then pop an item from hte dictionary and print it
	key, value = dictionary.popitem()
	stripe = make_stripe(key, value)
	#reduce dictionary size accordinbly
	size -= len(value)
	print(stripe)

#print items in the dictionary
def print_items():
    for key, value in dictionary.items():
    	print(make_stripe(key, value)) 

for line in sys.stdin:

    
    line = line.strip()
    tokens = line.split()
    

    #the key is the context - the first two words
    key = tokens[0] + " " + tokens[1]
    #only send the number of occurences of the word
    occurences = tokens[3]
    #increment value in primary dictionary
    incr_primary_dict(key, occurences)
    #check if overstepped memory bounds
    check_full()

    #considered printing after parsing each line, but do not do it because word pairs might repeat in next lines

#print remainign items
print_items()    	
	
