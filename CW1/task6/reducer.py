#!/usr/bin/python

import sys
import math

#currently considered context
prev_context = ""
prev_sum = 0

#a string to which I append the strings of occurence counts
words = "" 


def calculate_entropy():

    counts = []
	
    #split the string and parse it into integers
    for word in words.split():
	counts.append(int(word))

    #calculate the sum of the numbers
    prev_sum = sum(counts)

	
    result = 0.0

    #calculate the entropy
    for word in counts:
	#cast to float to make sure it is not inreger division
	frequency = int(word) / float(prev_sum)
	#"add" to result
     	result -= math.log(frequency,2) * frequency  

    return result

#print entropy result for current ("prev") context
def print_entropy():
    entropy = calculate_entropy()
    #print output as per requirements
    print '%s %s' % (prev_context, str(entropy))	

for line in sys.stdin:

    line = line.strip()
    tokens = line.split('\t')
    #know that tab is the separator
    context = tokens[0]

    

    #if it is a new context and it is not an initial one
    if context != prev_context and not (prev_context ==  ""):
	#print entropy for current context and reset variables
	print_entropy()
	words = ""
    
    #set the context and append the new values to the string            
    prev_context = context
    #use a separator to make sure the numbers do not stick to each other to form incorrect values (we parse the spaces away afterwards anyway using split() )    
    words += " " + tokens[1]
	
#print the remaining records
print_entropy()




