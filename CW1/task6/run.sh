#!/bin/bash
hdfs dfs -rm -r /user/s1346981/assignment1/task6/
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
-D mapred.job.name="Task 6 s1346981" \
-input /user/s1346981/assignment1/task4/ \
-output /user/s1346981/assignment1/task6/ \
-mapper mapper.py \
-reducer reducer.py \
-combiner combiner.py \
-file mapper.py \
-file reducer.py \
-file combiner.py 

