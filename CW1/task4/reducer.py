#!/usr/bin/python

import sys
import json

#do not need a dictionary of dictionaries as we know input sorted by key
#so can simply store in a dictionary of key = 3rd word, value = count for currently considered first 2 words
dictionary = dict()

#the first two words
prev_key =  ""


#increment value by given amount in dictionary or add it
def incr_dict(dictionary, key, value):

	if key in dictionary:
	      dictionary[key]+= value
	else:
	      dictionary[key] = value

#print current ("prev") key and associated dictionary
def print_items():
	for key, value in dictionary.items():
		print_item(key, value)

#make the reducer output the final output format
def print_item(key, value):
	#make the output as per requirement
	triplet = prev_key + " " + key
	print("%s\t%s" % (triplet, value))


for line in sys.stdin:

    line = line.strip()
    tokens = line.split('\t')
   
    #the first two words
    key = tokens[0]

    #if it is a new key and it is not an initial one, then the items
    if key != prev_key and not (prev_key ==  ""):
	#print currently accumulated values and clear dictionary for new key
	print_items()
	dictionary.clear() 
                
    prev_key = key    

    #json loads for easier parsing
    associated_dictionary = json.loads(tokens[1])

    #increment corresponding values in the dictionary
    for word, count in associated_dictionary.items():
	incr_dict(dictionary, word, int(count)) 
	
#print the remaining records
print_items()




