#!/usr/bin/python

import sys
import json

#do not need a dictionary of dictionaries as we know input sorted by key
#so can simply store in a dictionary of key = 3rd word, value = count for currently considered first 2 words
dictionary = dict()

#the first two words
prev_key =  ""

#make a stripe of the key(first two words) and the 3rd word count dictionary
def make_stripe(key, values):
	result = key + "\t"
	#first two words are the key
	result += json.dumps(values)
	return result 

#increment value by given amount in dictionary or add it
def incr_dict(dictionary, key, value):

	if key in dictionary:
	      dictionary[key]+= value
	else:
	      dictionary[key] = value

#print current ("prev") key and associated dictionary
def print_items():
	print(make_stripe(prev_key, dictionary))

for line in sys.stdin:

    line = line.strip()
    #split by tab for as we know the first two words are delimited by this
    tokens = line.split('\t')
    key = tokens[0]

    #if it is a new key and it is not an initial one, then the items
    if key != prev_key and not (prev_key ==  ""):
	#print currently accumulated values and clear dictionary for new key
	print_items()
	dictionary.clear() 
    
    #record the new key            
    prev_key = key    

    #json loads for easier parsing
    associated_dictionary = json.loads(tokens[1])

    #increment corresponding values in the dictionary
    for word, count in associated_dictionary.items():
	incr_dict(dictionary, word, int(count)) 
	
#print the remaining records
print_items()




