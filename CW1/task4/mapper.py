#!/usr/bin/python

import sys
import json

#maximum size of the dictionary
MAX_SIZE = 1000

#current size of the dictionary
size = 0

#dictionary to that holds the first two words as key and a dictionary (of 3rd word occurence counts) as value
dictionary = dict()

#get the dictionary size (in number of records)
def dictionary_size(dictionary):
	result = sys.getsizeof(dictionary)
	return result

#make a stripe of the key(first two words) and their associated dictionary of 3rd word and their count(value)
def make_stripe(key, values):
	#print with tab as the separator so hadoop sorts by the first two words
	result = key + "\t"
	#use json to save formatting time
	result += json.dumps(values)
	return result 

#increment value in dictionary or add to it (generalized method)
def incr_dict(dictionary, key, value):
        global size
	if key in dictionary:
	      #increment
	      dictionary[key]+= value
	else:
	      #add new record
	      dictionary[key] = value
	      #record size increase
	      size += 1

#increment or add value into the dictionary of dictionaries
def incr_primary_dict(key, value):
	if key in dictionary:
	      #increment the associated dictionary held as the value (by 1 as reading from file)
	      incr_dict(dictionary[key], value, 1)
	else:
	      #create a new dictionary to be held as value and add the 3rd word to it with count 1
	      dictionary[key] = dict()
	      incr_dict(dictionary[key], value, 1)

def check_full():
    #only keep a bounded number of records in memory
    global size
    if  size > MAX_SIZE:
	#if size too big, then pop an item from hte dictionary and print it
	key, value = dictionary.popitem()
	stripe = make_stripe(key, value)
	#reduce dictionary size accodingly
	size -= len(value)
	print(stripe)

#print items in the dictionary
def print_items():
    for key, value in dictionary.items():
    	print(make_stripe(key, value)) 

for line in sys.stdin:

    
    line = line.strip()
    tokens = line.split()
    

    #get the 3-word tokens
    for index in xrange(0, len(tokens) - 2):

	#key is the first two words
	key =   tokens[index] + " " + tokens[index+1]  
	word3 = tokens[index+2] 
	#increment the 3rd word occurence count for the first two
	incr_primary_dict(key, word3)
	#check if overstepped memory bounds
	check_full()

    #considered printing after parsing each line, but do not do it because word pairs might repeat in next lines

#print remainign items
print_items()    	
	


